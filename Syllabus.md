### Syllabus

#### Lecturer

* Name: Dr. Francesco Mambrini
* Email: [name].[surname]@uni-leipzig.de
* Twitter: @FrancMambr

#### Office Hours

Tuesdays 13:00 - 15:00, Paulinum Raum P-618 (appointment only)

* GoogleHangout: f.mambrini@gmail.com (appointment only)
* Skype: francescomambrini (appointment only)

#### Class Resources

* **GitLab**: [https://git.informatik.uni-leipzig.de/introdh17-18](https://git.informatik.uni-leipzig.de/introdh17-18)
* **OLAT**: [https://olat.informatik.uni-leipzig.de/](https://olat.informatik.uni-leipzig.de/)

#### Expectations for Class Participation

* Physical presence in the lectures
* Preparation for and active participation in the seminars
* The assignments given in the seminars are due 24h before the next seminar
* Handing-in of the project report by March 01, 2017 (Prüfungsleistung)

#### Venues

* V: Lecture, Felix-Klein-Hörsaal P501 P5.014, Mondays 1.15-2.45pm
* S: Seminar,
	* Group 1, Paulinum, P-801, Tuesdays, 3.15-4.45pm
	* Group 2, Paulinum, P-801, Mondays, 9.15-10.45am
	* Group 3, Paulinum, P-801, Mondays, 11.15-12.45am
* P: Praktikum (Vorlesungsfreie Zeit)

#### Schedule
**Note**: the content and title of the lessons may still change.

| Number | Form | Topic | Date | Notes |
| ------ | ---- | ----- | ---- | ----- |
| 1 | V | Welcome by Prof. Gregory Crane / Housekeeping / Introduction: What are the Digital Humanities? | 9.10 ||
| 2 | S | (Some) Tools of the trade: Git, OS, VM... | 10.10 (g1), 16.10 (g2,3) | The first date refers to Group 1; the second to Groups 2 and 3 |
| 3 | V | Sailing the Big Ocean: Data and Metada for DH| 16.10 ||
| 4 | S | Getting your Data: Introduction to data formats, APIs, and data cleaning | 17.10, 23.10 |
| 5 | V | Introduction to programming and programming languages | 23.10 |
| 6 | S | Getting started with Python | 24.10, 30.10 | |
| 7 | V | Digital editions and digital curations. Standards and practices (TEI, Dublin Core) | 30.10 |
| 8 | S | An overview of DH projects. Discussion of ideas | 6.11 (g2,3), 28.11 (g1) | Group 1 will have this session on Nov. 28 |
| 9 | V | Guest lecture: New frontiers of DH. Games and gamification (Parker Crane) | 6.11 | 
| 10 | S | Working with data in XML | 7.11, 13.11 |
| 11 | V | Language and Textual Corpora | 13.11 |
| 12 | S | Exploring Corpora with Python | 14.11, 20.11 |
| 13 | V | More Fun with Languages: names, styles, sentiments | 20.11 |
| 14 | S | NLP with Python NLTK | 21.11, 27.11 |
| 15 | V | Telling stories with data: data analysis and visualization | 27.11 |
| 16 | S | Data Analysis and Visualization in R (and Python) | 5.12, 11.12 |
| 17 | V | Models, sites and images: data and problems of digital archaeology | 11.12 |
| 18 | S | Data sources and resources for digital archaeology. Working with the iDAI.world | 12.12, 18.12 |
| 19 | V | How to plan and build your DH project | 18.12 |
| 20 | P | Non-classroom project work | - |
| 21 | V | Of links and networks: introduction to Network Analysis | 8.1 |
| 22 | S | Tools for Network Analysis. Introduction to Gephi and Graph Databases | 9.1, 15.1 |
| 23 | V | Guest presentation: Working with Geodata (Chiara Palladino) | 15.1 | Preliminary title |
| 24 | S | Marking and mapping geodata | 16.1, 22.1 | Preliminary title |
| 25 | V | Reading Close and Far:  Introduction to Topic Modelling | 22.1 |  |
| 26 | S | Topic modelling | 23.1, 29.1 |
| 27 | V | Wrapping all up! Conclusions and feedback | 29.1 |


